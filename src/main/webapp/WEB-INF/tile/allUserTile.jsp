<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 29.08.2017
  Time: 18:35
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <div class="container">
        <div class="row">
            <div class="panel-group category-products">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>UserId</th>
                            <th>FirstName</th>
                            <th>LastName</th>
                            <th>Sex</th>
                            <th>Country</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Pass</th>
                            <th></th>

                            <th> <input type="checkbox" unchecked></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${userList}" var="userList">
                            <tr>
                                <td><c:out value="${userList.getId()}"/></td>
                                <td><c:out value="${userList.getFirstName()}"/></td>
                                <td><c:out value="${userList.getLastName()}"/></td>
                                <td><c:out value="${userList.getSex()}"/></td>
                                <td><c:out value="${userList.getCountry()}"/></td>
                                <td><c:out value="${userList.getPhone()}"/></td>
                                <td><c:out value="${userList.getEmail()}"/></td>
                                <td><c:out value="${userList.getPass()}"/></td>
                                <td><a href="/deleteUser?id=${userList.getId()}" class="action-icon"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                    <a href="/editUser?id=${userList.getId()}" class="action-icon"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <td> <input type="checkbox" unchecked></td>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

</section>

