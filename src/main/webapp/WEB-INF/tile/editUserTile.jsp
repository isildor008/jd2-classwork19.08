<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  UserValid: Dmitry
  Date: 04.09.2017
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form">
                    <h2>Edit User</h2>
                    <form:form action="editUserForm" id="id" modelAttribute="user">
                        <form:hidden path="id"/>

                        <div class="form-group">
                            <label>First Name:</label>
                            <form:input id="firstName" type="text" name="firstName" placeholder="firstName"
                                        path="firstName"/>
                            <form:errors path="firstName" cssClass="error"/>
                        </div>

                        <div class="form-group">
                            <label>Last Name:</label>
                            <form:input id="lastName" placeholder="lastName" type="text" name="lastName"
                                        path="lastName"/>
                            <form:errors path="lastName" cssClass="error"/>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-2">Sex:</label>
                            <div class="radio col-xs-3">
                                <label><form:radiobutton path="sex" value="Male"/>Male</label>
                            </div>
                            <div class="radio col-xs-3">
                                <label><form:radiobutton path="sex" value="Female"/>Female</label>
                            </div>
                            <form:errors path="sex" cssClass="error"/>
                        </div>

                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label>Country:</label>
                            <form:select path="country">
                                <form:option value="NONE" label="--- Select ---"/>
                                <form:options items="${listCountry}" itemValue="name" itemLabel="name"/>
                            </form:select>
                            <form:errors path="country" cssClass="error"/>
                        </div>


                        <div class="form-group">
                            <label>Phone:</label>
                            <form:input id="phone" placeholder="phone"  name="phone" path="phone"/>
                            <form:errors path="phone" cssClass="error"/>
                        </div>


                        <div class="form-group">
                            <label>Email:</label>
                            <form:input id="email" placeholder="email" type="email" name="email" path="email"/>
                            <form:errors path="email" cssClass="error"/>
                        </div>


                        <div class="form-group">
                            <label>Password:</label>
                            <form:input id="pass" placeholder="pass" type="pass" name="pass" path="pass"/>
                            <form:errors path="pass" cssClass="error"/>
                        </div>

                        <button type="submit " class="btn btn-default">Submit</button>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</section>


