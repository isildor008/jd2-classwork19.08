<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 05.06.2017
  Time: 12:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer id="footer"><!--Footer-->
    <div class="container">

            <ul class="footer-nav">
                <li><a href="/about"> About</a></li>
                <li><a href="tel:375298919962"><i class="fa fa-phone"></i> +375 29 891 99 62</a></li>
                <li><a href="mailto:"><i class="fa fa-envelope"></i> Dmitry Pateev</a></li>
            </ul>
    </div>
</footer>
<!--/Footer-->