<%--
  Created by IntelliJ IDEA.
  User: Dmitry
  Date: 06.06.2017
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header id="header"><!--header-->


    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="/addUser"><i class="glyphicon glyphicon-plus"></i> add new User</a></li>
                            <li><a href="/allUsers">All Users</a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div><!--/header_top-->


</header>
