package by.htp.myloginpage.dao;


/**
 * Created by Dmitry on 28.08.2017.
 */
public interface BaseDao {
    void save(Object o);

    void update(Object o);

    void delete(Object o);


}
