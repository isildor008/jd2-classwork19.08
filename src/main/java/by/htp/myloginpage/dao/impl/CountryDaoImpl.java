package by.htp.myloginpage.dao.impl;

import by.htp.myloginpage.dao.CountryDao;
import by.htp.myloginpage.entity.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dmitry on 28.08.2017.
 */
@Repository
public class CountryDaoImpl extends BaseDaoImpl implements CountryDao {
   private static final SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

    public List<Country> getAll() {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            List list = session.createCriteria(Country.class).list();
            session.getTransaction().commit();
            return list;
        } finally {
            session.close();
        }
    }
}
