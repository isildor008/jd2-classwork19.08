package by.htp.myloginpage.dao.impl;

import by.htp.myloginpage.dao.UserDao;
import by.htp.myloginpage.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dmitry on 28.08.2017.
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl implements UserDao {
    private static final SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

    public List<User> getAll() {

        Session session = factory.openSession();
        try {
            session.beginTransaction();
            List list = session.createCriteria(User.class).list();
            session.getTransaction().commit();
            return list;
        } finally {
            session.close();
        }
    }

    public User getById(int id) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            User user=session.get(User.class,id);
            session.getTransaction().commit();
            return user;
        } finally {
            session.close();
        }

    }
}
