package by.htp.myloginpage.dao.impl;


import by.htp.myloginpage.dao.BaseDao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


/**
 * Created by Dmitry on 28.08.2017.
 */
public class BaseDaoImpl implements BaseDao{

     private static final SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();


    public void save(Object o) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            session.save(o);
            session.getTransaction().commit();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            session.close();
        }
    }


    public void update(Object o) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            session.update(o);
            session.getTransaction().commit();
        } finally {
            session.close();
        }

    }

    public void delete(Object o) {
        Session session = factory.openSession();
        try {
            session.beginTransaction();
            session.delete(o);
            session.getTransaction().commit();
        }finally {
            session.close();
        }
    }


}

