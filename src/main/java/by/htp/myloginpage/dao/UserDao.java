package by.htp.myloginpage.dao;

import by.htp.myloginpage.entity.User;

import java.util.List;

/**
 * Created by Dmitry on 28.08.2017.
 */
public interface UserDao extends BaseDao {

    List<User> getAll();

    User getById(int id);
}
