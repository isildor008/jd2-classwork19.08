package by.htp.myloginpage.dao;

import by.htp.myloginpage.entity.Country;

import java.util.List;

/**
 * Created by Dmitry on 28.08.2017.
 */
public interface CountryDao extends BaseDao {

    List<Country> getAll();
}
