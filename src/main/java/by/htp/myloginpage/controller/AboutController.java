package by.htp.myloginpage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Dmitry on 05.09.2017.
 */
@Controller
public class AboutController {

    @RequestMapping("/about")
    public String about(){
        return "/about";
    }

}
