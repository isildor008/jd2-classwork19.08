package by.htp.myloginpage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexPageCommand {

	@RequestMapping("/")
	public String showForm() {
		return "redirect:/allUsers";
	}

}
