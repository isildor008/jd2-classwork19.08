package by.htp.myloginpage.controller;

import by.htp.myloginpage.dao.CountryDao;
import by.htp.myloginpage.dao.UserDao;
import by.htp.myloginpage.domain.UserValid;
import by.htp.myloginpage.entity.Country;
import by.htp.myloginpage.entity.User;
import by.htp.myloginpage.service.UserToEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller

public class UserController {

    @Autowired
    UserDao userDao;

    @Autowired
    CountryDao countryDao;


    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {

        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }

    @RequestMapping("/allUsers")
    public String allUsers(Model model) {
        List<User> userList = userDao.getAll();
        model.addAttribute("userList", userList);

        return "/allUserPage";
    }

    @RequestMapping("/addUser")
    public String addUser(Model model, Model country) {

        model.addAttribute("user",new UserValid());
        List<Country> list = countryDao.getAll();
        country.addAttribute("listCountry", list);
        return "/addUserPage";
    }

    @RequestMapping("/addUserForm")
    public String processForm(@Valid @ModelAttribute("user") UserValid userValid, BindingResult theBindingResult, Model country) {
        if (theBindingResult.hasErrors()) {
            List<Country> list = countryDao.getAll();
            country.addAttribute("listCountry", list);
            return "/addUserPage";
        } else {
            List<Country> list = countryDao.getAll();
            country.addAttribute("listCountry", list);
            userDao.save(UserToEntity.convert(userValid));
            return "redirect:/allUsers";
        }
    }

    @RequestMapping("/deleteUser")
    public String deleteUser(@RequestParam int id) {
        User user = userDao.getById(id);
        userDao.delete(user);
        return "redirect:/allUsers";
    }

    @RequestMapping("/editUser")
    public String editUser(@RequestParam int id, Model model, Model country) {
        User user = userDao.getById(id);
        model.addAttribute("user", user);
        List<Country> list = countryDao.getAll();
        country.addAttribute("listCountry", list);
        return "/editUserPage";
    }

    @RequestMapping("/editUserForm")
    public String editUserForm(@Valid @ModelAttribute("user") User user, BindingResult theBindingResult, Model country) {
        if (theBindingResult.hasErrors()) {
            List<Country> list = countryDao.getAll();
            country.addAttribute("listCountry", list);
            return "/editUserPage";
        } else {
            List<Country> list = countryDao.getAll();
            country.addAttribute("listCountry", list);
            userDao.update(user);
            return "redirect:/allUsers";
        }
    }


}
