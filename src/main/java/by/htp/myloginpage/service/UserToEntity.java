package by.htp.myloginpage.service;


import by.htp.myloginpage.domain.UserValid;
import by.htp.myloginpage.entity.User;


/**
 * Created by Dmitry on 05.09.2017.
 */
public class UserToEntity {

    private UserToEntity(){}

    public static User convert(UserValid userValid){
        User user=new User();
        user.setCountry(userValid.getCountry());
        user.setEmail(userValid.getEmail());
        user.setFirstName(userValid.getFirstName());
        user.setLastName(userValid.getLastName());
        user.setPass(userValid.getPass());
        user.setPhone(userValid.getPhone());
        user.setSex(userValid.getSex());
        return user;
    }
}
